<?php

  namespace Roots\Sage\Init;

  use Roots\Sage\Assets;

  /**
   * Theme setup
   */
  function setup() {
    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
            'top_bar' => __('Top Bar', 'sage'),
            'primary_navigation' => __('Primary Navigation', 'sage'),
            'footer_navigation' => __('Footer Navigation', 'sage'),
            'sub_footer_navigation' => __('Sub Footer Navigation', 'sage'),
            'sitemap' => __('Sitemap', 'sage')
    ]);

    // Add post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');

    // Add post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Add HTML5 markup for captions
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list']);

    // Tell the TinyMCE editor to use a custom stylesheet
    add_editor_style(Assets\asset_path('styles/editor-style.css'));
  }
  add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

  /**
   * Register sidebars
   */
  function widgets_init() {
    register_sidebar([
            'name'          => __('Primary', 'sage'),
            'id'            => 'sidebar-primary',
            'before_widget' => '<section class="widget %1$s %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>'
    ]);

    register_sidebar([
            'name'          => __('Drawer', 'sage'),
            'id'            => 'sidebar-drawer',
            'before_widget' => '<section class="widget %1$s %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>'
    ]);
    /**
     * Custom Sidebars
     */
    if( have_rows('ic_cw_repeater', 'options') ):
      $wCount = 0;
      while( have_rows('ic_cw_repeater', 'options') ):
        the_row();
        // vars
        $wCount++;
        $wName = get_sub_field('ic_cw_name', 'options');
        $wNameSan = preg_replace("/^[\w]+$/", "-", $wName);
        register_sidebar([
                'name'          => __($wName, 'sage'),
                'id'            => $wNameSan,
                'before_widget' => '<section class="widget %1$s %2$s ' .$wNameSan.'">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3>',
                'after_title'   => '</h3>'
        ]);
      endwhile;
    endif;
  }
  add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');



   
// INCLUDE ACF PRO IN THEME - THIS IS NOT MEANT FOR ANY KIND OF UNAUTHORIZED REDISTRIBUTION
// 1. customize ACF path
add_filter('acf/settings/path', __NAMESPACE__ . '\\ic_acf_settings_path');
function ic_acf_settings_path( $path ) {
  // update path
  $path = get_stylesheet_directory() . '/plugin/advanced-custom-fields-pro/';
  // return
  return $path;
}
// 2. customize ACF dir
add_filter('acf/settings/dir', __NAMESPACE__ . '\\ic_acf_settings_dir');
function ic_acf_settings_dir( $dir ) {
  // update path
  $dir = get_stylesheet_directory_uri() . '/plugin/advanced-custom-fields-pro/';
  // return
  return $dir;
}
// 3. Hide ACF field group menu item
// add_filter('acf/settings/show_admin', __NAMESPACE__ . '\\__return_false');

// 4. Include ACF
include_once( get_stylesheet_directory() . '/plugin/advanced-custom-fields-pro/acf.php' );

// ADD ACF FACEBOOK GALLERY FIELD PLUGIN
// 1. customize ACF Facebook path
add_filter('acf/facebook/settings/path', __NAMESPACE__ . '\\ic_acf_facebook_settings_path');
function ic_acf_facebook_settings_path( $path ) {
  // update path
  $path = get_stylesheet_directory() . '/plugin/acf-facebook-gallery-field/';
  // return
  return $path;
}
// 2. customize ACF dir
add_filter('acf/facebook/settings/dir', __NAMESPACE__ . '\\ic_acf_facebook_settings_dir');
function ic_acf_facebook_settings_dir( $dir ) {
  // update path
  $dir = get_stylesheet_directory_uri() . '/plugin/acf-facebook-gallery-field/';
  // return
  return $dir;
}
// 3. Hide ACF field group menu item
// add_filter('acf/facebook/settings/show_admin', __NAMESPACE__ . '\\__return_false');

// 4. Include ACF
  include_once( get_stylesheet_directory() . '/plugin/acf-facebook-gallery-field/acf-facebook_gallery.php');

// ADD ACF WIDGET AREA FIELD PLUGIN
// 1. customize ACF Widget path
add_filter('acf/widget/settings/path', __NAMESPACE__ . '\\ic_acf_widget_settings_path');
function ic_acf_widget_settings_path( $path ) {
  // update path
  $path = get_stylesheet_directory() . '/plugin/advanced-custom-fields-widget-area-field/';
  // return
  return $path;
}
// 2. customize ACF dir
add_filter('acf/widget/settings/dir', __NAMESPACE__ . '\\ic_acf_widget_settings_dir');
function ic_acf_widget_settings_dir( $dir ) {
  // update path
  $dir = get_stylesheet_directory_uri() . '/plugin/advanced-custom-fields-widget-area-field/';
  // return
  return $dir;
}
// 3. Hide ACF field group menu item
// add_filter('acf/widget/settings/show_admin', __NAMESPACE__ . '\\__return_false');

// 4. Include ACF
include_once( get_stylesheet_directory() . '/plugin/advanced-custom-fields-widget-area-field/acf-widget-area.php');


// TODO DO I NEED TO ADD THE MENU PICKER HERE TOO
// Create options page for ACF theme options.
//---------------------------------------------------------------
if( function_exists('acf_add_options_page') ) {
acf_add_options_page(array(
        'page_title' 	=> 'Site General Settings',
        'menu_title'	=> 'Site Settings',
        'menu_slug' 	=> 'site-general-settings',
        'capability'	=> 'manage_options',
        'redirect'		=> false
));
}

