<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


// OS and browser detection classes
function mv_browser_body_class($classes) {
  global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
  if($is_lynx) $classes[] = 'lynx';
  elseif($is_gecko) $classes[] = 'gecko';
  elseif($is_opera) $classes[] = 'opera';
  elseif($is_NS4) $classes[] = 'ns4';
  elseif($is_safari) $classes[] = 'safari';
  elseif($is_chrome) $classes[] = 'chrome';
  elseif($is_IE) {
    $classes[] = 'ie';
    if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
      $classes[] = 'ie'.$browser_version[1];
  } else $classes[] = 'unknown';
  if($is_iphone) $classes[] = 'iphone';
  if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
    $classes[] = 'osx';
  } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
    $classes[] = 'linux';
  } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
    $classes[] = 'windows';
  }
  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\mv_browser_body_class');

// Use Bootstrap Responsive Embed for videos in content
function bootstrap_wrap_oembed( $html ){
  $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
  return'<div class="embed-responsive embed-responsive-16by9">'.$html.'</div>';
}
add_filter( 'embed_oembed_html', __NAMESPACE__ . '\\bootstrap_wrap_oembed',10,1);


// Add logo and url to login page from theme options
function my_login_logo_one() {
  $logoObject = get_field('ic_ss_bs_pl', 'options');
  $logoUrl = $logoObject['url'];
  ?>
  <style type="text/css">
    body.login{
      background-color:<?php the_field('ic_s_ub_color',
                  'options') ?>;
    <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubiurl', 'options')){?>
      background-image:url('<?php the_field('ic_s_ubiurl', 'options') ?>');
    <?php } ?>
    <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubit', 'options') ==
            'repeat-y') {?>
      background-repeat:<?php the_field('ic_s_ubit', 'options') ?>;
      background-size: 100% auto;
    <?php } ?>
    <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubit', 'options') ==
            'repeat-x') {?>
      background-repeat:<?php the_field('ic_s_ubit', 'options') ?>;
      background-size: auto 100%;
    <?php } ?>
    <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubit', 'options') ==
            'cover'){?>
      background-size:<?php the_field('ic_s_ubit', 'options') ?>;
      background-repeat:no-repeat;
    <?php } ?>
    }
    body.login div#login h1 a {
      background-image: url(<?php if ( $logoUrl != '' ) { echo $logoUrl; } else { bloginfo('name'); }; ?>); background-size:contain; width:100%; height:150px; }
    <?php if(get_field('bkg_txt_color', 'options') == 'cm-text-light'){?>
    body.login #backtoblog a, body.login #nav a{
      color:#fff;
    }
    <?php }?>
  </style>
  <?php
}
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo_one' );

function my_login_logo_url() {
  return home_url();
}
add_filter( 'login_headerurl', __NAMESPACE__ . '\\my_login_logo_url' );

function my_login_logo_url_title() {
  $blogname = get_bloginfo('name');
  return $blogname;
}
add_filter( 'login_headertitle', __NAMESPACE__ . '\\my_login_logo_url_title' );


// Convert hexdec color string to rgb(a) string
// TODO USE THIS HELP GENERATE ALL OF THE COLOR VARIABLES IN THE THEME STLYING
/* useage:
$color = '#ffa226';
$rgb = hex2rgba($color);
$rgba = hex2rgba($color, 0.7);
*/
function hex2rgba($color, $opacity = false) {
  $default = 'rgb(0,0,0)';
  //Return default if no color provided
  if(empty($color))
    return $default;
  //Sanitize $color if "#" is provided
  if ($color[0] == '#' ) {
    $color = substr( $color, 1 );
  }
  //Check if color has 6 or 3 characters and get values
  if (strlen($color) == 6) {
    $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
  } elseif ( strlen( $color ) == 3 ) {
    $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
  } else {
    return $default;
  }
  //Convert hexadec to rgb
  $rgb =  array_map('hexdec', $hex);
  //Check if opacity is set(rgba or rgb)
  if($opacity){
    if(abs($opacity) > 1)
      $opacity = 1.0;
    $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
  } else {
    $output = 'rgb('.implode(",",$rgb).')';
  }
  //Return rgb(a) color string
  return $output;
}


//CUSTOM POST ID FUNCTION FOR IN OR OUTSIDE THE LOOP
/* TODO NOT CURRENTLY WORKING BUT I LIKE THE IDEA
function get_the_post_id() {
  if (in_the_loop()) {
    $post_id = get_the_ID();
  } else {
    global $wp_query;
    $post_id = $wp_query->get_queried_object_id(); // this isn't quite right
  }
  return $post_id;
}
*/



/* PROBALBLE #ZOMBIECODE
// Add SVG to uploads
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\\cc_mime_types');
*/

//* Make link button checkbox
function overwrite_wplink() {
  // Disable wplink
  wp_deregister_script( 'wplink' );

  // Register a new script file to be linked
  wp_register_script( 'wplink', get_stylesheet_directory_uri() . '/assets/scripts/wplink.min.js', array( 'jquery', 'wpdialogs' ), false, 1 );
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\overwrite_wplink', 999 );

//add new wordpress image size
add_image_size( 'xLarge', 1200, 600, false );

//TODO DETAIL DESCRIPTIONS AND INSTRUCTIONS FOR FEILDS - THIS WILL NEED AN OUTSIDE LOOK
//TODO PULL THESE OUT INTO SEPERATE FILES
// ADDING CONTENT MODULE BASE FIELDS
if( function_exists('acf_add_local_field_group') ):
  $gallery_source_id = get_field('ic_si_ss_fbid', 'options');
  $access_token = get_field('ic_si_ss_fbat', 'options');
  acf_add_local_field_group(array (
    'key' => 'group_57b85634b2fab',
    'title' => 'Content Module Base',
    'fields' => array (
      array (
        'key' => 'field_57b865253be59',
        'label' => 'Content Type',
        'name' => 'type',
        'type' => 'select',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
                'width' => '33',
                'class' => '',
                'id' => '',
        ),
        'choices' => array (
                'text' => 'Text',
                'image' => 'Image',
                'video' => 'Video',
                'gallery' => 'Custom Gallery',
                'fbGallery' => 'Facebook Gallery',
                'widget' => 'Widget Area',
        ),
        'default_value' => array (
                0 => 'text',
        ),
        'allow_null' => 0,
        'multiple' => 0,
        'ui' => 0,
        'ajax' => 0,
        'return_format' => 'value',
        'placeholder' => '',
      ),
      array (
        'key' => 'field_57b865d73be5b',
        'label' => 'Transparent Background',
        'name' => 'bkg',
        'type' => 'true_false',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
                'width' => '33',
                'class' => '',
                'id' => '',
        ),
        'message' => '',
        'default_value' => 0,
      ),
      array (
        'key' => 'field_57b8663f3be5c',
        'label' => 'Pop up in lightbox',
        'name' => 'lightbox',
        'type' => 'true_false',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => array (
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'image',
            ),
          ),
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'video',
            ),
          ),
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'gallery',
            ),
          ),
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'fbGallery',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '33',
          'class' => '',
          'id' => '',
        ),
        'message' => '',
        'default_value' => 0,
      ),
      array (
        'key' => 'field_57b866a43be5e',
        'label' => 'Image',
        'name' => 'image',
        'type' => 'image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => array (
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'image',
            ),
          ),
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'video',
            ),
            array (
              'field' => 'field_57b8663f3be5c',
              'operator' => '==',
              'value' => '1',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '50',
          'class' => '',
          'id' => '',
        ),
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
      ),
      array (
        'key' => 'field_57b867073be61',
        'label' => 'Video	Embed URL',
        'name' => 'video_url',
        'type' => 'text',
        'instructions' => 'Copy the url from the iframe embed share option.',
        'required' => 0,
        'conditional_logic' => array (
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'video',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '50',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_57b867643be62',
        'label' => 'Custom Gallery',
        'name' => 'gallery',
        'type' => 'gallery',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => array (
          array (
            array (
              'field' => 'field_57b865253be59',
              'operator' => '==',
              'value' => 'gallery',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'min' => '',
        'max' => '',
        'insert' => 'prepend',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
      ),
      array (
        'key' => 'field_57b867ac3be63',
        'label' => 'Facebook Gallery',
        'name' => 'fbGallery',
        'type' => 'facebook_gallery',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => array (
          array (
            array (
                'field' => 'field_57b865253be59',
                'operator' => '==',
                'value' => 'fbGallery',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'gallery_source_id' => $gallery_source_id,
        'access_token' => $access_token,
        'cache_images' => 3,
        'font_size' => 14,
      ),
      array (
              'key' => 'field_57b867f83be64',
              'label' => 'Text Content',
              'name' => 'text',
              'type' => 'wysiwyg',
              'instructions' => '',
              'required' => 0,
              'conditional_logic' => array (
                      array (
                              array (
                                      'field' => 'field_57b865253be59',
                                      'operator' => '!=',
                                      'value' => 'widget',
                              ),
                      ),
              ),
              'wrapper' => array (
                      'width' => '',
                      'class' => '',
                      'id' => '',
              ),
              'default_value' => '',
              'tabs' => 'all',
              'toolbar' => 'full',
              'media_upload' => 1,
      ),
      array (
              'key' => 'field_57b86a2e9edd2',
              'label' => 'Widget Area',
              'name' => 'widget',
              'type' => 'widget_area',
              'instructions' => '',
              'required' => 0,
              'conditional_logic' => array (
                      array (
                              array (
                                      'field' => 'field_57b865253be59',
                                      'operator' => '==',
                                      'value' => 'widget',
                              ),
                      ),
              ),
              'wrapper' => array (
                      'width' => '',
                      'class' => '',
                      'id' => '',
              ),
              'allow_null' => 1,
              'display_or_return' => 'return',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'post',
        ),
      ),
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'page',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 0,
    'description' => '',
  ));
endif;
// ADDING HOURS FIELDS BASE
if( function_exists('acf_add_local_field_group') ):
  acf_add_local_field_group(array (
    'key' => 'group_57c1c1be24cd4',
    'title' => 'Hours Base',
    'fields' => array (
      array (
        'key' => 'field_57c1c1dc2c2c6',
        'label' => 'Open',
        'name' => 'hd_open',
        'type' => 'true_false',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '15',
          'class' => '',
          'id' => '',
        ),
        'message' => '',
        'default_value' => 1,
      ),
      array (
        'key' => 'field_57c1c21d2c2c7',
        'label' => 'Hours',
        'name' => 'hd_hours',
        'type' => 'repeater',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => array (
          array (
            array (
              'field' => 'field_57c1c1dc2c2c6',
              'operator' => '==',
              'value' => '1',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '85',
          'class' => '',
          'id' => '',
        ),
        'collapsed' => '',
        'min' => 1,
        'max' => 3,
        'layout' => 'block',
        'button_label' => 'Add Shift',
        'sub_fields' => array (
          array (
            'key' => 'field_57c1c2312c2c8',
            'label' => 'Opening Time',
            'name' => 'hd_opening_time',
            'type' => 'time_picker',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array (
              array (
                array (
                  'field' => 'field_57c1c1dc2c2c6',
                  'operator' => '==',
                  'value' => '1',
                ),
              ),
            ),
            'wrapper' => array (
              'width' => '50',
              'class' => '',
              'id' => '',
            ),
            'display_format' => 'g:i a',
            'return_format' => 'H:i:s',
          ),
          array (
            'key' => 'field_57c1c2432c2c9',
            'label' => 'Closing Time',
            'name' => 'hd_closing_time',
            'type' => 'time_picker',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array (
              array (
                array (
                  'field' => 'field_57c1c1dc2c2c6',
                  'operator' => '==',
                  'value' => '1',
                ),
              ),
            ),
            'wrapper' => array (
              'width' => '50',
              'class' => '',
              'id' => '',
            ),
            'display_format' => 'g:i a',
            'return_format' => 'H:i:s',
          ),
        ),
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'site-general-settings',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 0,
    'description' => '',
  ));
endif;