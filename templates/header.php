<?php
  namespace Roots\Sage\Nav;
  use Roots\Sage\Nav\NavWalker;

  $businessName = get_field('ic_ss_bi_bn', 'options');

  $primaryLogo = "";
  $primaryLogo = get_field('ic_ss_bs_pl', 'options');
  $primaryLogoUrl = $primaryLogo['url'];
  
  $mobileLogo = "";
  $mobileLogo = get_field('ic_ss_bs_ml', 'options');
  $mobileLogoUrl = $mobileLogo['url'];

  $bPhone = get_field('ic_ss_bi_bmp', 'options');

  $heroBanner = get_field('ic_s_hb', 'options' );
  $heroBannerUrl = $heroBanner['url'];
  
  // Logo fall backs
  // Header
  $headerLogoUrl = "";
  if($mobileLogoUrl != ""){
    $headerLogoUrl = $mobileLogoUrl;
  }elseif( $primaryLogoUrl != ""){
    $headerLogoUrl = $primaryLogoUrl;
  }
  // Drawer
  if($mobileLogoUrl != ""){
    $drawerLogoUrl = $mobileLogoUrl;
  }elseif($primaryLogoUrl != ""){
    $drawerLogoUrl = $primaryLogoUrl;
  }else{
    $drawerLogoUrl = ""; //this means we'll skip the section if none of these are set.
  }

  // For getting post ID on every page. #zombiecode
  include('page-id.php');
?>
<div id="ic-header-wrap" class="mdl-js-layout">
  <header class="mdl-layout--fixed-header mdl-layout__header mdl-layout__header--transparent">
    <div class="mdl-layout__header-row">
      <!-- LOGO WITH FALLBACK CONDITION -->
      <div id="mobile-Logo">
        <a href="<?php echo esc_url(home_url('/')); ?>">
          <?php if ($headerLogoUrl != ""){?>
              <img src="<?php echo $headerLogoUrl; ?>" />
          <?php } else { ?>
              <h1><?php echo $businessName; ?></h1>
          <?php } ?>
        </a>
      </div>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <?php // OPEN LIGHT with admin condition
        if(get_field('ic_ss_bi_os', 'options')) {
          get_template_part('templates/openlight');
        }
      ?>
      <?php
        // NAVIGATION - TOP BAR MENU LOCATION
        if ( has_nav_menu( 'top_bar' ) ) :
          // Remove wrapping <li> from around links
          // https://css-tricks.com/snippets/wordpress/remove-li-elements-from-output-of-wp_nav_menu/#comment-542093
          $cleanermenu = wp_nav_menu( array(
                  'theme_location' => 'top_bar',
                  'container' => false,
                  'items_wrap' => '<nav class="mdl-navigation top_bar">%3$s</nav>',
                  'echo' => false,
                  'depth' => 1,
          ) );
          $find = array('><a','<li');
          $replace = array('','<a');
          echo str_replace( $find, $replace, $cleanermenu );
        endif;
      ?>
      <?php //TODO CREATE AN ADMIN CONDITION AROUND THIS - ALSO DRAWR AND FOOTER
        // get_template_part('templates/social');
      ?>
    </div>


  </header>
  <div class="mdl-layout__drawer">
    <?php get_template_part('templates/hours-text'); ?>
    <?php /* TODO MAKE THIS AN OPTION IN THE SETTINGS
    <div id="drawer-mobile-Logo">
      <a href="<?php echo esc_url(home_url('/')); ?>">
        <?php if ($drawerLogoUrl != ""){?>
          <img src="<?php echo $drawerLogoUrl; ?>" />
        <?php } else { ?>
          <h1><?php echo $businessName; ?></h1>
        <?php } ?>
      </a>
    </div>
    */ ?>
    <?php dynamic_sidebar('sidebar-drawer'); ?>
    <?php
      if ( has_nav_menu( 'top_bar' ) ) :
        // Remove wrapping <li> from around links
        // https://css-tricks.com/snippets/wordpress/remove-li-elements-from-output-of-wp_nav_menu/#comment-542093
        $cleanermenu = wp_nav_menu( array(
                'theme_location' => 'top_bar',
                'container' => false,
                'items_wrap' => '<nav class="mdl-navigation top_bar">%3$s</nav>',
                'echo' => false,
                'depth' => 1,
        ) );
        $find = array('><a','<li');
        $replace = array('','<a');
        echo str_replace( $find, $replace, $cleanermenu );
      endif;
    ?>
    <div id="drawer-footer">
      <?php get_template_part('templates/phone-dir'); ?>
      <?php get_template_part('templates/address'); ?>
      <?php get_template_part('templates/social'); ?>
    </div>
  </div>
</div>
<!-- HERO BANNER - LOGO - HOME LINK - HOURS - ADDRESS -->
<div class="mdl-layout">
  <div class="banner-wrap">
    <a class="em-layout-transparent hero-banner" <?php
      if($heroBannerUrl != ""){ ?>
       style="background-image: url('<?php echo $heroBannerUrl; ?>');"<?php
      } ?>
       href="<?php echo esc_url(home_url('/')); ?>"
       title="<?php echo $businessName; ?>"><?php
        if($primaryLogoUrl != ''){ ?>
          <img class="primaryLogo" src="<?php echo $primaryLogoUrl ;?>" alt="<?php echo $businessName; ?>"/>
      <?php } else{ ?>
      <h1><?php echo $businessName; ?></h1>
      <?php }?>
    </a>

      <?php get_template_part('templates/hours-text'); ?>
      <?php get_template_part('templates/address'); ?>

  </div>
</div>
