<?php
  //IMAGE FIELD
  $imgObj = $module['image'];
  $imgUrl = $imgObj['url'];
  $imgTitle = $imgObj['title'];
  $imgAlt = $imgObj['alt'];
    // image size
    $size = 'xLarge';
    $xLarge = $imgObj['sizes'][ $size ];
    $width = $imgObj['sizes'][ $size . '-width' ];
    $height = $imgObj['sizes'][ $size . '-height' ];
  $figureUrl = $xLarge;
  //LIGHTBOX FIELD
  $lightbox = $module['lightbox'];
  //VIDEO URL FIELD
  $videoTypeUrl = $module['video_url'];
  //GALLERY
  $gallery = "";
  if ($layoutType == 'gallery'){
    $gallery = $module['gallery'];
  }elseif ($layoutType == 'fbGallery'){
    $gallery = $module['fbGallery'];
  }
  //WIDGET FIELD
  $widget = $module['widget'];

$wNameSan = preg_replace("#[^-a-zA-Z0-9]+#", "-", $widget);
$widget = $wNameSan . $wCount;
  //TEXT CONTENT FIELD
  $text = $module['text'];

  if( $layoutType == 'image' || $layoutType == 'video'){
    include('cm-figure.php');
  } elseif ($layoutType == 'gallery' || $layoutType == 'fbGallery'){
    include('cm-gallery.php');
  } elseif ($layoutType == 'widget'){
    include('cm-widgets.php');
  } elseif ($layoutType == 'text'){
    include('cm-text.php');
  } else {
    // do nothing
  }











