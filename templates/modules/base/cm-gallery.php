<?php
  if( $gallery ):
?>
    <div id="slider_" class="flexslider slider">
      <ul class="slides">
        <?php foreach( $gallery as $image ):
          $gallery = "";
          if ($layoutType == 'gallery'){
            $imageURL = $image['url'];
          }elseif ($layoutType == 'fbGallery'){
            $imageURL = $image['src'];
          }?>
          <li>
            <?php if( $lightbox === true ) {?>
              <a href="<?php if( $lightbox === true ) { echo $imageURL; }else{}?>" class="<?php if( $lightbox === true ) { echo 'gallery_lightbox'; }else{}?>" style="background-image: url(<?php echo $imageURL; ?>);"></a>
            <?php } else { ?>
              <figure class="cm-<?php echo $layoutType; ?>-figure" style="background-image: url(<?php echo $imageURL; ?>);"></figure>
            <?php } ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
<?php
  endif;
?>
