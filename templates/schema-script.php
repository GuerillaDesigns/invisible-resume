<?php ?>
<script type="application/ld+json">
{
  	"@context": "http://schema.org",
  	"@type": "LocalBusiness",

  	<?php if(get_field("ic_ss_bi_bn", "option")){ ?>
	"name": "<?php the_field("ic_ss_bi_bn", "option"); ?>",
	<?php }else{ ?>
  	"name": "<?php bloginfo('name'); ?>",
  	<?php } ?>

  	"url": "<?php bloginfo('url'); ?>",

  	<?php if(get_field("ic_ss_bs_pl", "option")){ ?>
	"logo": "<?php $logoUrl = get_field("ic_ss_bs_pl", "option"); echo $logoUrl['url']; ?>",
	<?php } ?>

  	<?php if(get_field("ic_ss_bi_bd", "option")){?>
	"description": "<?php the_field("ic_ss_bi_bd", "option"); ?>",
	<?php } ?>

  	<?php if(get_field("ic_ss_bi_bmp", "option")){?>
	"telephone": "<?php the_field("ic_ss_bi_bmp", "option"); ?>",
	<?php } ?>
	<?php
	if(get_field('ic_ss_bi_bh_mon', 'options')) {
		$monday = get_field('ic_ss_bi_bh_mon', 'options');
		$monHours = $monday['hd_hours'];
		foreach($monHours as $shift) {
			$shiftOpen = $shift['hd_opening_time'];
			$shiftClose = $shift['hd_closing_time'];
			$hours = $shiftOpen . '-' . $shiftClose;
			echo '"openingHours": "Mo, ' . $hours . '",';
		}
	}
	if(get_field('ic_ss_bi_bh_tues', 'options')) {
		$tuesday = get_field('ic_ss_bi_bh_tues', 'options');
		$tuesHours = $tuesday['hd_hours'];
		foreach($tuesHours as $shift) {
			$shiftOpen = $shift['hd_opening_time'];
			$shiftClose = $shift['hd_closing_time'];
			$hours = $shiftOpen . '-' . $shiftClose;
			echo '"openingHours": "Tu, ' . $hours . '",';
		}
	}
	if(get_field('ic_ss_bi_bh_wed', 'options')) {
		$wednesday = get_field('ic_ss_bi_bh_wed', 'options');
		$wedHours = $wednesday['hd_hours'];
		foreach($wedHours as $shift) {
			$shiftOpen = $shift['hd_opening_time'];
			$shiftClose = $shift['hd_closing_time'];
			$hours = $shiftOpen . '-' . $shiftClose;
			echo '"openingHours": "We, ' . $hours . '",';
		}
	}
	if(get_field('ic_ss_bi_bh_thur', 'options')) {
		$thursday = get_field('ic_ss_bi_bh_thur', 'options');
		$thurHours = $thursday['hd_hours'];
		foreach($thurHours as $shift) {
			$shiftOpen = $shift['hd_opening_time'];
			$shiftClose = $shift['hd_closing_time'];
			$hours = $shiftOpen . '-' . $shiftClose;
			echo '"openingHours": "Th, ' . $hours . '",';
		}
	}
	if(get_field('ic_ss_bi_bh_fri', 'options')) {
		$friday = get_field('ic_ss_bi_bh_fri', 'options');
		$friHours = $friday['hd_hours'];
		foreach($friHours as $shift) {
			$shiftOpen = $shift['hd_opening_time'];
			$shiftClose = $shift['hd_closing_time'];
			$hours = $shiftOpen . '-' . $shiftClose;
			echo '"openingHours": "Fr, ' . $hours . '",';
		}
	}
	if(get_field('ic_ss_bi_bh_sat', 'options')) {
		$saturday = get_field('ic_ss_bi_bh_sat', 'options');
		$satHours = $saturday['hd_hours'];
		foreach($satHours as $shift) {
			$shiftOpen = $shift['hd_opening_time'];
			$shiftClose = $shift['hd_closing_time'];
			$hours = $shiftOpen . '-' . $shiftClose;
			echo '"openingHours": "Sa, ' . $hours . '",';
		}
	}
	if(get_field('ic_ss_bi_bh_sun', 'options')) {
		$sunday = get_field('ic_ss_bi_bh_sun', 'options');
		$sunHours = $sunday['hd_hours'];
		foreach($sunHours as $shift) {
			$shiftOpen = $shift['hd_opening_time'];
			$shiftClose = $shift['hd_closing_time'];
			$hours = $shiftOpen . '-' . $shiftClose;
			echo '"openingHours": "Su, ' . $hours . '",';
		}
	}
	?>
<?php
		if( have_rows('ic_ss_bi_bh', 'option') ):
    while ( have_rows('ric_ss_bi_bh', 'option') ) :
		the_row();?>
			"openingHours": "<?php the_sub_field('ic_ss_bi_bh_d', 'option'); ?>,<?php the_sub_field('ic_ss_bi_bh_d', 'option'); ?>-<?php the_sub_field('ic_ss_bi_bh_d', 'option');?>",
		<?php endwhile;
		else :
    // no rows found
		endif;
		?>

  	<?php if(get_field("ic_ss_bi_gclat", "option")){ ?>
  	"geo": {
	    "@type": "GeoCoordinates",
		    "<?php the_field("ic_ss_bi_gclat", "option"); ?>",
		    "<?php the_field("ic_ss_bi_gclon", "option"); ?>"
	},
	<?php } ?>
    <?php if(get_field("ic_ss_bi_ba", "option")){ ?>
        "address": {
        "@type": "PostalAddress",
           <?php if(get_field("ic_ss_bi_ba", "option")){?>
            "streetAddress": "<?php the_field("ic_ss_bi_ba", "option"); ?>"
           <?php } ?>
           <?php if(get_field("ic_ss_bi_bc", "option")){?>
            "addressLocality": "<?php the_field("ic_ss_bi_bc", "option"); ?>",
           <?php } ?>
           <?php if(get_field("ic_ss_bi_bs", "option")){?>
            "addressRegion": "<?php the_field("ic_ss_bi_bs", "option"); ?>",
           <?php } ?>
           <?php if(get_field("ic_ss_bi_bz", "option")){?>
            "postalCode": "<?php the_field('ic_ss_bi_bz', 'option'); ?>"
           <?php } ?>
        },
    <?php } ?>

  	<?php /*
 			 if(get_field("sld_award", "option")){ ?>
	"logo": "<?php the_field("sld_award", "option"); ?>",
	<?php } */
 				?>
	<?php if( have_rows('ic_ss_bi_owp', "option") ) : ?>
		"sameAs" : [
		<?php while ( have_rows('ic_ss_bi_owp', "option") ) : ?>
			<?php the_row();?>
				<?php if(get_sub_field("ic_ss_bi_owp_url", "option")){?>
					"<?php the_sub_field("ic_ss_bi_owp_url", "option"); ?>",
				<?php } ?>
		<?php endwhile; ?>
		],
	<?php endif; ?>
	<?php if( have_rows('ic_ss_bi_cps', "option") ) :  ?>
		"contactPoint" : [
		<?php while ( have_rows('ic_ss_bi_cps', "option") ) : ?>
			<?php the_row();?>
			{ "@type" : "ContactPoint",
			<?php if(get_sub_field("ic_ss_bi_cpt", "option")){?>
				"contactType" : "<?php the_sub_field("ic_ss_bi_cpt", "option"); ?>",
			<?php } ?>
			<?php if(get_sub_field("ic_ss_bi_cpp", "option")){?>
				"telephone" : "<?php the_sub_field("ic_ss_bi_cpp", "option"); ?>",
			<?php } ?>
			<?php if(get_sub_field("ic_ss_bi_cpe", "option")){?>
				"email" : "<?php the_sub_field("ic_ss_bi_cpe", "option"); ?>",
			<?php } ?>
			}
		<?php endwhile; ?>
		],
	<?php endif; ?>

}
</script>