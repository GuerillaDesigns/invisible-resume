<?php
  $bAddress = get_field('ic_ss_bi_ba', 'options');
  $bCity = get_field('ic_ss_bi_bc', 'options');
  $bState = get_field('ic_ss_bi_bs', 'options');
  $bZip = get_field('ic_ss_bi_bz', 'options');
  $bGmap = get_field('ic_ss_bi_bmap', 'options');
  $bDirections = get_field('ic_ss_bi_bdirections', 'options');
  $blog_title = get_bloginfo();
  $searchTerm = preg_replace('/\s+/', '+', $blog_title);
  ?>
<div id="bAddress">
  <a href="<?php
    if(wp_is_mobile() && $bGmap != ''){
      echo $bGmap;
    } elseif ($bDirections != ''){
      echo $bDirections;
    }else{
      echo "http://www.google.com/maps/place/$searchTerm+$bAddress+$bCity+$bState+$bZip";
    }?>" <?php if((wp_is_mobile() && $bGmap != '') || (wp_is_mobile() && $bGmap == '' && $bDirections == ''))
    {?>target="_blank"<?php }?>><span><?php echo
  $bAddress; ?></span> <span><?php echo $bCity; ?>, <?php echo $bState; ?>  <?php echo $bZip; ?></span></a>

</div>