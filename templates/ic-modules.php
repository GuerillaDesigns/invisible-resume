<?php
  use Roots\Sage\Config;
  use Roots\Sage\Wrapper;
  // TODO WRITE A NOTE ABOUT INCLUSION AND EDITING OF THE MODULE FILES IN THE CHILD THEME -
  //  INCLUDE ALL OF THEM IF ANY AND THEN EDIT AS NEEDED
?>
<?php
$mods_used = [];
$mods_show = [];
$child_path = get_stylesheet_directory() . '/templates/modules/';
$parent_path = get_template_directory() . '/templates/modules/';
foreach(glob($child_path . '*.php') as $filename) {
  $mods_used[] = str_replace($child_path, "", $filename);
  $mods_show[] = $filename;
}
foreach(glob($parent_path . '*.php') as $filename) {
  $mods_this = str_replace($parent_path, "", $filename);
  if(!in_array($mods_this, $mods_used)) {
    $mods_show[] = $filename;
  }
}
if(have_rows('content_modules')) :
  $mod_count = 0; // Number modules rendered to page
  $head_count = 0; // Number headers on page for tags

  //TODO DECIDE IF AND HOW A SIDEBAR WILL WORK / IS NECESSARY
  //TODO CREATE SETTINGS FOR SIDEBAR LOCATION - LEFT - RIGHT - BOTTOM
  if (Config\display_sidebar()) :
    $col = 'mdl-cell--8-col';
  else :
    $col = 'mdl-cell--12-col';
  endif;
  ?>
  <section id="ic_modules_wrap" class="mdl-cell <?php echo $col; ?>">
    <div id="ic_modules">
  <?php while(have_rows('content_modules')) : $mod_count++;
    the_row();
    $txt_color = get_field('bkg_txt_color', 'options');
    foreach($mods_show as $filename) {
        include $filename;
    } endwhile; ?>
    </div>
  </section>
<?php else : endif; ?>
