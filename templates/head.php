<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php the_field('ic_ss_bs_fi', 'option'); ?>" type="image/x-icon"/>
  <?php get_template_part('templates/schema', 'script'); ?>
  <?php // get_template_part('templates/schema', 'script'); ?>
  <?php if(get_field('ic_s_hs', 'options')){
    the_field('ic_s_hs', 'options');
  }?>
  <?php wp_head(); ?>
</head>
