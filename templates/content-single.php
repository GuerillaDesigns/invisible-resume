<?php while (have_posts()) : the_post();
  // TODO STYLE PAGINATION
  // TODO MAKE SURE 404, ARCHIVES, CATEGORIES ALL DISPLAY PROPERLY

  setup_postdata( $post );
  //Build post thumbnail url's
  $post_thumbnail_id = get_post_thumbnail_id($post->ID);
  $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
  $videoUrl = get_field('ic_post_video_url');
  ?>
<div class="the_content post_content">
  <article <?php post_class();
    //TODO WRITE A CONDITION TO CHECK FOR VIDEO URL AND DISPLAY VIDEO IF IT EXISTS
  ?>>
    <?php if ($post_thumbnail_url != ""){?>
      <figure class="post_featured_img">
        <img src="<?php echo $post_thumbnail_url;?>"/>
        <a href="<?php echo $post_thumbnail_url;?>" class="fa fa-expand popup-image cm-posts-link post-figure-expand"></a>
      </figure>
    <?php }?>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php //get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
      <?php get_template_part('templates/ic-modules'); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php // comments_template('/templates/comments.php'); ?>
  </article>
  </div>
<?php endwhile; ?>
